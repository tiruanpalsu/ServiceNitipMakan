package com.nitipmakan.controller;


import java.util.Date;
import java.util.List;


import com.nitipmakan.model.Pesanan;
import com.nitipmakan.model.Status;
import com.nitipmakan.util.HibernateUtil;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping(path="/nitipmakan")
public class PesananController {
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/pesan", method = RequestMethod.POST)
	public List<Pesanan> insertPesanan(@RequestBody List<Pesanan> pesanan) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		
		List<Pesanan> result = session.createQuery("from Pesanan where pengguna.nim='"+pesanan.get(0).getPengguna().getNim()+"' order by transaksi asc").list();
		int last;
		if(result.size() > 0){
			last=Integer.parseInt(result.get(result.size()-1).getTransaksi().substring(9));
			last++;
		}else {
			last=1;
		}
		
		Date date = new Date();
		for(Pesanan kirim:pesanan){
			Status status = new Status();
			status.setId(0);
			kirim.setStatus(status);
			kirim.setTanggal(date);
			kirim.setTransaksi(kirim.getPengguna().getNim()+Integer.toString(last));
			session.save(kirim);
		}
		
		session.getTransaction().commit();
		session.close();
		return pesanan;
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(path="/pesanan", method = RequestMethod.POST)
	public List<Pesanan> getAllPesanan(@RequestParam(value="nim") String nim) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		List<Pesanan> result = session.createQuery("from Pesanan where pengguna.nim='"+nim+"' order by status_id asc").list();
		session.getTransaction().commit();
		session.close();
		return result;
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(path="/detailpesanan", method = RequestMethod.POST)
	public List<Pesanan> getDetailPesanan(@RequestParam(value="transaksi") String transaksi) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		List<Pesanan> result = session.createQuery("from Pesanan where transaksi='"+transaksi+"' order by transaksi asc").list();
		session.getTransaction().commit();
		session.close();
		return result;
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(path="/pesanankurir", method = RequestMethod.POST)
	public List<Pesanan> getPesananKurir(@RequestParam(value="kurir_nim") String kurir_nim) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		
		List<Pesanan> result = session.createQuery("from Pesanan where status_id = 0 and not substring(transaksi,1,9) = '"+kurir_nim+"' order by transaksi asc").list();
		
		
		session.getTransaction().commit();
		session.close();
		return result;
	} 	 
	
	@SuppressWarnings("unchecked")
	@RequestMapping(path="/pesanankurir/todo", method = RequestMethod.POST)
	public List<Pesanan> getTodoListKurir(@RequestParam(value="kurir_nim") String kurir_nim) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		
		List<Pesanan> result = session.createQuery("from Pesanan where kurir_nim ='"+kurir_nim+"' and status_id=1 order by transaksi asc").list();
		
		session.getTransaction().commit();
		session.close();
		return result;
	} 
	

	@SuppressWarnings("unchecked")
	@RequestMapping(path="/pickup", method = RequestMethod.POST)
	public void setStatusPesanan1(@RequestParam(value="transaksi") String transaksi,@RequestParam(value="kurir_nim") String kurir_nim) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		
		List<Pesanan> result = session.createQuery("from Pesanan where transaksi='"+transaksi+"' order by transaksi asc").list();
		if(result.get(0).getStatus().getId()==0){
			session.createQuery("update Pesanan set kurir_nim ='"+kurir_nim+"', status_id=1 where transaksi ='"+transaksi+"'").executeUpdate();
		}	
		
		session.getTransaction().commit();
		session.close();
		
	}
	
	@RequestMapping(path="/pickup/berhasil", method = RequestMethod.POST)
	public void setStatusPesanan2(@RequestParam(value="transaksi") String transaksi) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();

		List<Pesanan> result = session.createQuery("from Pesanan where transaksi='"+transaksi+"' order by transaksi asc").list();
		if(result.get(0).getStatus().getId()==1){
			session.createQuery("update Pesanan set status_id =2 where transaksi ='"+transaksi+"'").executeUpdate();
		}
		
		session.getTransaction().commit();
		session.close();
		
	}
	

	@RequestMapping(path="/batalpesanan", method = RequestMethod.POST)
	public void setStatusPesanan3(@RequestParam(value="transaksi") String transaksi) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();

		session.createQuery("delete from Pesanan where transaksi ='"+transaksi+"'").executeUpdate();
		
		session.getTransaction().commit();
		session.close();
		
	}
}


