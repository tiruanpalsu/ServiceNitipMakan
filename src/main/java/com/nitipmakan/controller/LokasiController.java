package com.nitipmakan.controller;

import java.util.List;

import com.nitipmakan.model.Lokasi;
import com.nitipmakan.util.HibernateUtil;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping(path="/nitipmakan")
public class LokasiController {
	
	@SuppressWarnings("unchecked")
	@RequestMapping(path="/lokasi", method = RequestMethod.POST)
	public List<Lokasi> getAllPesanan(@RequestParam(value="query") String query) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		List<Lokasi> result = session.createQuery("FROM Lokasi WHERE query LIKE '%"+query.toLowerCase()+"%'").list();
		session.getTransaction().commit();
		session.close();
		return result;
	}
	
}

