package com.nitipmakan.controller;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.nitipmakan.model.Makanan;
import com.nitipmakan.util.HibernateUtil;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping(path="/nitipmakan")
public class MakananController {
	
	@SuppressWarnings("unchecked")
	@GetMapping(path="/makanan/{id}/{page}")
	public List<Makanan> getMakanan(@PathVariable String id, @PathVariable String page) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();

		List<Makanan> terpilih = new ArrayList<Makanan>();
		List<Makanan> result = session.createQuery("from Makanan where warung.id="+id).list();
		for(Makanan sementara:result){
			if(sementara.getId().substring(0,2).equals("mk")){
				terpilih.add(sementara);
			}
		}
		session.getTransaction().commit();
		session.close();
		return terpilih;
	}

	@SuppressWarnings("unchecked")
	@GetMapping(path="/minuman/{id}/{page}")
	public List<Makanan> getMinuman(@PathVariable String id,@PathVariable String page) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();

		List<Makanan> terpilih = new ArrayList<Makanan>();
		List<Makanan> result = session.createQuery("from Makanan where warung.id="+id).list();
		for(Makanan sementara:result){
			if(sementara.getId().substring(0,2).equals("mn")){
				terpilih.add(sementara);
			}
		}
		session.getTransaction().commit();
		session.close();
		return terpilih;
	}

	
}
