package com.nitipmakan.controller;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nitipmakan.model.Pengguna;
import com.nitipmakan.util.HibernateUtil;


@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping(path="/nitipmakan")
public class PenggunaController {
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public Pengguna login(@RequestBody Login login) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		List<Pengguna> result = session.createQuery("from Pengguna where nim='"+login.getNim()+"'").list();
		session.getTransaction().commit();
		session.close();
		Pengguna hasil = new Pengguna();
		if(result.size()==1){
			if(result.get(0).getPassword().equals(login.getPassword())){
				hasil=result.get(0);
			}
		}
		return hasil;
		
	}
	
	@SuppressWarnings("unchecked")
	 @RequestMapping(value="/register", method = RequestMethod.POST)
	 public boolean register(@RequestBody Pengguna pengguna){
	  Session session = HibernateUtil.getSessionFactory().openSession();
	  session.beginTransaction();
	  
	  boolean hasil = false;
	  List<Pengguna> result = session.createQuery("from Pengguna where nim='"+pengguna.getNim()+"'").list();
	  if(result.size()==0){
		  session.save(pengguna);
		  hasil = true;
	  }
	  session.getTransaction().commit();
	  session.close();
	  return hasil;
	 }
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/info", method = RequestMethod.POST)
	 public List<Pengguna> infoKurir(@RequestParam(value="nim") String nim){
	  Session session = HibernateUtil.getSessionFactory().openSession();
	  session.beginTransaction();
	  List<Pengguna> result = session.createQuery("from Pengguna where nim='"+nim+"'").list();
	  session.getTransaction().commit();
	  session.close();
	  return result;
	 }
	
	
}

class Login {
	private String nim;
	private String password;
	private String role;
	public String getNim() {
		return nim;
	}
	public void setNim(String nim) {
		this.nim = nim;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
}