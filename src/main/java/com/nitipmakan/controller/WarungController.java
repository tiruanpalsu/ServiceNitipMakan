package com.nitipmakan.controller;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.nitipmakan.model.Warung;
import com.nitipmakan.util.HibernateUtil;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping(path="/nitipmakan")
public class WarungController {
	
	@SuppressWarnings("unchecked")
	@GetMapping(path="/warung")
	public List<Warung> getAllWarung() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		List<Warung> result = session.createQuery("from Warung").list();
		session.getTransaction().commit();
		session.close();
		return result;
		
	}

}