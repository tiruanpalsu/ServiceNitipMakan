package com.nitipmakan.model;

import java.util.Date;

import javax.persistence.*;

@Entity
public class Pesanan {
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	private String transaksi;
	
	private int jumlah;
	
	private Date tanggal;
	
	@ManyToOne
    private Makanan makanan;
	
	@ManyToOne
    private Pengguna pengguna;
    
    @ManyToOne
    private Pengguna kurir;
    
    @ManyToOne
    private Lokasi lokasi;
    
    @ManyToOne
    private Status status;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTransaksi() {
		return transaksi;
	}

	public void setTransaksi(String transaksi) {
		this.transaksi = transaksi;
	}

	public int getJumlah() {
		return jumlah;
	}

	public void setJumlah(int jumlah) {
		this.jumlah = jumlah;
	}

	public Date getTanggal() {
		return tanggal;
	}

	public void setTanggal(Date tanggal) {
		this.tanggal = tanggal;
	}

	public Makanan getMakanan() {
		return makanan;
	}

	public void setMakanan(Makanan makanan) {
		this.makanan = makanan;
	}

	public Pengguna getPengguna() {
		return pengguna;
	}

	public void setPengguna(Pengguna pengguna) {
		this.pengguna = pengguna;
	}

	public Pengguna getKurir() {
		return kurir;
	}

	public void setKurir(Pengguna kurir) {
		this.kurir = kurir;
	}

	public Lokasi getLokasi() {
		return lokasi;
	}

	public void setLokasi(Lokasi lokasi) {
		this.lokasi = lokasi;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}
	
	
}

