package com.nitipmakan.model;

import javax.persistence.*;

@Entity
public class Pengguna {
    @Id
    private String nim;
    
    private String nama;
    
    private String prodi;
    
    private String notelp;
    
    private String idline;
    
    private String password;
    
	public String getNim() {
		return nim;
	}

	public void setNim(String nim) {
		this.nim = nim;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getProdi() {
		return prodi;
	}

	public void setProdi(String prodi) {
		this.prodi = prodi;
	}

	public String getNotelp() {
		return notelp;
	}

	public void setNotelp(String notelp) {
		this.notelp = notelp;
	}

	public String getIdline() {
		return idline;
	}

	public void setIdline(String idline) {
		this.idline = idline;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
}

