package com.nitipmakan.model;

import javax.persistence.*;

@Entity
public class Makanan {
    @Id
    private String id;
    
    private String nama;
    
    private int harga;
    
    @ManyToOne
    private Warung warung;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public int getHarga() {
		return harga;
	}

	public void setHarga(int harga) {
		this.harga = harga;
	}

	public Warung getWarung() {
		return warung;
	}

	public void setWarung(Warung warung) {
		this.warung = warung;
	}
	
}

